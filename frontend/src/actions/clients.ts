import { Action, Client, ClientBasic } from "../types";

export const SET_CLIENTS = "SET_CLIENTS";
export const ADD_CLIENT = "ADD_CLIENT";
export const SEARCH_CLIENT = "SEARCH_CLIENT";

export const setClients = (clients: Client[]): Action => {
  return {
    type: SET_CLIENTS,
    payload: clients,
  };
};

export const addClient = (client: ClientBasic): Action => {
  return {
    type: ADD_CLIENT,
    payload: client,
  };
};

export const searchClient = (clientName: string): Action => {
  return {
    type: SEARCH_CLIENT,
    payload: clientName,
  };
};
