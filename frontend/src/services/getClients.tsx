import { Client } from "../types";

export async function getClients(): Promise<Client[]> {
  const response = await fetch("./clients.json");
  const clients = await response.json();
  return clients;
}
