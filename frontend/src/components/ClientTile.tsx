import React, { FC } from "react";
import { Client } from "../types";
import "./ClientTile.css";

interface ClientTileProps {
  client: Client;
}

const ClientTile: FC<ClientTileProps> = ({ client }: ClientTileProps) => {
  const { name, email, company, dateCreated, image } = client;
  return (
    <div className="c-client-tile card">
      <div className="image">
        <img src={image} />
      </div>
      <div className="content">
        <a className="header">{name}</a>
        <div className="description">{company}</div>
        <div className="description">{email}</div>
        <div className="description">{`Added on: ${dateCreated}`}</div>
      </div>
    </div>
  );
};

export default ClientTile;
