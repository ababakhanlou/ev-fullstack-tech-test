import React, { useEffect, useState } from "react";
import { Button, Header, Modal } from "semantic-ui-react";
import { Dispatch } from "redux";
import { useDispatch } from "react-redux";
import { addClient as addClientAction } from "../actions/clients";
import "./AddClient.css";

const AddClient = (): JSX.Element => {
  const [open, setOpen] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [inputName, setInputName] = useState<string>("");
  const [inputCompany, setInputCompany] = useState<string>("");
  const [inputEmail, setInputEmail] = useState<string>("");

  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    if (!open) {
      setInputName("");
      setInputCompany("");
      setInputEmail("");
      setError(false);
    }
  }, [open]);

  const addClient = () => {
    if (inputName && inputCompany && inputEmail) {
      dispatch(
        addClientAction({
          name: inputName,
          company: inputCompany,
          email: inputEmail,
        })
      );
      setOpen(false);
    } else {
      setError(true);
    }
  };

  return (
    <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={
        <Button className="c-add-client__button green">Add Client</Button>
      }
    >
      <Modal.Content image>
        <Modal.Description>
          <Header>Add a new client</Header>
          <div className="ui form">
            <div className="required field">
              <label>Name</label>
              <input
                type="text"
                placeholder="Full Name"
                value={inputName}
                onChange={(e) => setInputName(e.target.value)}
              />
            </div>
            <div className="required field">
              <label>Company</label>
              <input
                type="text"
                placeholder="Company"
                value={inputCompany}
                onChange={(e) => setInputCompany(e.target.value)}
              />
            </div>
            <div className="required field">
              <label>Email</label>
              <input
                type="text"
                placeholder="Email"
                value={inputEmail}
                onChange={(e) => setInputEmail(e.target.value)}
              />
            </div>
          </div>
          {error && (
            <div className="ui error message">
              <div className="header">Please compelte all fields</div>
            </div>
          )}
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button color="red" onClick={() => setOpen(false)}>
          Cancel
        </Button>
        <Button
          content="Add"
          labelPosition="right"
          icon="checkmark"
          onClick={() => addClient()}
          positive
        />
      </Modal.Actions>
    </Modal>
  );
};

export default AddClient;
