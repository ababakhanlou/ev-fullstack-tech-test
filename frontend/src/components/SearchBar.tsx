import React, { useState, FormEvent } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { searchClient as searchClientAction } from "../actions/clients";

const SearchBar = (): JSX.Element => {
  const [inputSearch, setInputSearch] = useState<string>("");
  const dispatch: Dispatch = useDispatch();

  const onSubmitSearch = (e: FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    dispatch(searchClientAction(inputSearch));
  };

  return (
    <form data-testid="form" onSubmit={(e) => onSubmitSearch(e)}>
      <div className="ui fluid category search">
        <div className="ui icon input">
          <input
            className="prompt"
            type="text"
            placeholder="Search client..."
            value={inputSearch}
            onChange={(e) => setInputSearch(e.target.value)}
          />
          <i className="search icon"></i>
        </div>
        <div className="results"></div>
      </div>
    </form>
  );
};

export default SearchBar;
