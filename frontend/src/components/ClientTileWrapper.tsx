import React from "react";
import { useSelector } from "react-redux";
import { Client, ClientSearchResult } from "../types";
import { getClients, getClientsSearchResult } from "../selector/clients";
import ClientTile from "./ClientTile";
import "./ClientTileWrapper.css";

const ClientTileWrapper = (): JSX.Element => {
  const clientList: Client[] = useSelector(getClients);
  const clientsSearchResult: ClientSearchResult = useSelector(
    getClientsSearchResult
  );
  const list: Client[] = clientsSearchResult.searched
    ? clientsSearchResult.results
    : clientList;

  return (
    <div className="c-client-tile-wrapper ui link cards">
      {list.map((client) => (
        <ClientTile client={client} key={client.id} />
      ))}
      {!list.length && (
        <div className="ui error message">
          <div className="header">There are no clients of that name</div>
        </div>
      )}
    </div>
  );
};

export default ClientTileWrapper;
