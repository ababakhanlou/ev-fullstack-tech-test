import { Client, ClientSearchResult, State } from "../types";

export const getClients = (state: State): Client[] => {
  return state.clients;
};

export const getClientsSearchResult = (state: State): ClientSearchResult => {
  return state.clientSearchResult;
};
