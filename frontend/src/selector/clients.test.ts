import { getClients, getClientsSearchResult } from "./clients";
import clientData from "../../public/clients.json";

describe("getClients", () => {
  it("should return the clients from state", () => {
    const state = { clients: clientData };
    expect(getClients(state)).toBe(clientData);
  });
});

describe("getClientsSearchResult", () => {
  it("should return the clients of the search term from state", () => {
    const state = {
      clients: clientData,
      clientSearchResult: { results: [], searched: true },
    };
    expect(getClientsSearchResult(state)).toBe(state.clientSearchResult);
  });
});
