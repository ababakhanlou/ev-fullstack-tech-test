import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { Client } from "./types";
import { setClients as setClientsAction } from "./actions/clients";
import { getClients } from "./services/getClients";
import SearchBar from "./components/SearchBar";
import AddClient from "./components/AddClient";
import ClientTileWrapper from "./components/ClientTileWrapper";
import "./app.css";

const App = (): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  useEffect(() => {
    const getData = async (): Promise<void> => {
      const clients: Client[] = await getClients();
      dispatch(setClientsAction(clients));
    };
    getData();
  }, []);

  return (
    <div className="c-app">
      <AddClient />
      <SearchBar />
      <ClientTileWrapper />
    </div>
  );
};

export default App;
