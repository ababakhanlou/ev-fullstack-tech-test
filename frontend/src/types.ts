export interface Client {
  id: number;
  name: string;
  email: string;
  company: string;
  dateCreated: string;
  image: string;
}

export interface ClientBasic {
  name: string;
  email: string;
  company: string;
}

export interface ClientSearchResult {
  results: Client[];
  searched: boolean;
}

export interface State {
  clients: Client[];
  clientSearchResult: ClientSearchResult;
}

export interface Action {
  type: string;
  payload: any;
}
