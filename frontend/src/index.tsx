import React from "react";
import ReactDOM from "react-dom";
import App from "./app";
import { Provider } from "react-redux";
import { createStore, Store } from "redux";
import { reducer } from "./reducers/clients";
import { State, Action } from "./types";
import "./index.css";
import "semantic-ui-css/semantic.min.css";

const store: Store<State, Action> & {
  dispatch: any;
} = createStore(
  // @ts-ignore
  reducer,
  (window && (window as any)).__REDUX_DEVTOOLS_EXTENSION__ &&
    window &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
