import { reducer } from "./clients";
import clientData from "../../public/clients.json";

jest.mock("dayjs", () => () => ({
  format: () => "11/11/2011",
}));

describe("reducer", () => {
  beforeEach(() => {
    jest
      .spyOn(global.Math, "random")
      .mockReturnValue(0.111111)
      .mockReturnValueOnce(0.111);
  });

  afterEach(() => {
    jest.spyOn(global.Math, "random").mockRestore();
  });

  const initialState = {
    clients: [],
    clientSearchResult: { results: [], searched: false },
  };

  describe("SET_CLIENTS", () => {
    it("should return state with all clients", () => {
      expect(
        reducer(initialState, {
          type: "SET_CLIENTS",
          payload: clientData,
        })
      ).toStrictEqual({ ...initialState, clients: clientData });
    });
  });

  describe("ADD_CLIENT", () => {
    it("should return the new client with the original ones", () => {
      const newState = reducer(initialState, {
        type: "ADD_CLIENT",
        payload: {
          name: "john",
          company: "apple",
          email: "john@apple.com",
        },
      });
      expect(newState).toEqual({
        ...initialState,
        clients: [
          {
            name: "john",
            company: "apple",
            email: "john@apple.com",
            dateCreated: "11/11/2011",
            id: Math.floor(0.111111 * 999999),
            image: `https://i.pravatar.cc/${Math.floor(0.111 * 999)}`,
          },
        ],
      });
    });
  });

  describe("SEARCH_CLIENT", () => {
    it("should return new cleints with search term", () => {
      const state = {
        ...initialState,
        clients: clientData,
        clientSearchResult: { results: [], searched: true },
      };
      expect(
        reducer(state, {
          type: "SEARCH_CLIENT",
          payload: "zzz",
        })
      ).toStrictEqual({
        ...state,
        clientSearchResult: { results: [], searched: true },
      });
    });
  });
});
