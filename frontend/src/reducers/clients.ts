import { SET_CLIENTS, ADD_CLIENT, SEARCH_CLIENT } from "../actions/clients";
import { State, Action } from "../types";
import dayjs from "dayjs";

const initialState: State = {
  clients: [],
  clientSearchResult: { results: [], searched: false },
};

export const reducer = (state = initialState, action: Action): State => {
  switch (action.type) {
    case SET_CLIENTS: {
      return { ...state, clients: action.payload };
    }
    case ADD_CLIENT: {
      const newClient = {
        ...action.payload,
        image: `https://i.pravatar.cc/${Math.floor(Math.random() * 999)}`,
        dateCreated: `${dayjs().format("DD/MM/YYYY")}`,
        id: Math.floor(Math.random() * 999999),
      };
      return { ...state, clients: [...state.clients, newClient] };
    }
    case SEARCH_CLIENT: {
      const searchList = state.clients.filter((client) =>
        client.name.toLowerCase().includes(action.payload.toLowerCase())
      );
      return {
        ...state,
        clientSearchResult: {
          results: searchList,
          searched: Boolean(action.payload),
        },
      };
    }
    default:
      return state;
  }
};
